package me.damo1995.noenderdragon;

import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class DragonListener implements Listener {

	public static NoEnderDragon plugin;
	
	public DragonListener (NoEnderDragon instance)
	{
		plugin = instance;
	}
	
	@EventHandler
	public void onBlockExplode(EntityExplodeEvent event)
	{
		Entity e = event.getEntity();
		
		if (e instanceof EnderDragon && plugin.config.getBoolean("blockDamage"))
		{
			event.blockList().clear();
		}
	}
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event)
	{
		if (event.getEntity() instanceof EnderDragon && !plugin.config.getBoolean("spawnDragon"))
		{
			event.setCancelled(true);
		}
	}
	
}
