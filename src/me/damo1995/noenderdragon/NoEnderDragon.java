package me.damo1995.noenderdragon;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class NoEnderDragon extends JavaPlugin {

	public final Logger log = Logger.getLogger("Minecraft");
	//public DragonListener dragonListener = new DragonListener(this);
	
	public String pluginDirPath;
	public File configFile;
	public NoEnderDragonConfig config;
	
	public void onEnable()
	{
		getServer().getPluginManager().registerEvents(new DragonListener(this), this);
		
		this.pluginDirPath = this.getDataFolder().getAbsolutePath();
		this.configFile = new File(this.pluginDirPath + File.separator + "config.yml");
		this.config = new NoEnderDragonConfig(this.configFile);
		
		if (this.config.getBoolean("blockDamage") == false)
			this.log.log(Level.INFO, "Block Damage is disabled!");
		else
			this.log.log(Level.INFO, "Block Damage is enabled!");

		if (this.config.getBoolean("spawnDragon") == false)
			this.log.log(Level.INFO, "Dragon spawning is disabled!");
		else
			this.log.log(Level.INFO, "Dragon spawning is enabled!");
	}
	
	public void onDisable()
	{
		
	}
}
